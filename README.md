Docker build:

docker build . -t supchat-back

docker run -p 3000:3000 supchat-back

docker build . -t ivanfytsyk/supchat-back:1.0.2

docker push ivanfytsyk/supchat-back:1.0.2


List containers: **docker ps **

Attach: docker attach CONTAINER_ID

CTRL+C

docker pull ivanfytsyk/supchat-back:latest

docker run -p 3000:3000 ivanfytsyk/supchat-back:latest (on server)

Docker HUB: ivanfytsyk gtxtymrj

docker-compose up --build --force-recreate --no-deps - restart and recreate

В любой момент может понадобится
docker ps - список запущенных контейнеров
docker network ls - список всех активных и неактивных сетей созданных docker'ом
docker system prune - очистка всех остановленных контейнеров и неиспользуемых сетей
docker-compose down - выполняется из корня проекта. Останавливает запущенный контейнер.

Run locally:
docker-compose -f docker-compose.local.yml up --remove-orphans

**IMPRORTANT**
In case we resize server to start SSL renewal use *./init-letsencrypt.sh* as described here:
https://pentacent.medium.com/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71
