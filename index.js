const app = require('express')();

const cors = require('cors');

const fs = require("fs");

// use it before all route definitions
app.use(cors({origin: '*'}));
const bodyParser = require('body-parser');

app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
app.use(errorHandler);

const http = require('http').createServer(app);
const io = require('socket.io')(http);

const admin = require('firebase-admin');
const serviceAccount = require("./supchat-65619-firebase-adminsdk-iyb5k-f7a883fa29.json");

const path = require('path');

const FieldValue = require('firebase-admin').firestore.FieldValue;
const Timestamp = require('firebase-admin').firestore.Timestamp;

const geoip = require('geoip-lite');

const {uniqueNamesGenerator, adjectives, names} = require('unique-names-generator');

const nodemailer = require('nodemailer');

const multer = require('multer');
// SET STORAGE
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        console.log("I'm here!!!!11111111!!!===============================");
        cb(null, path.join(__dirname, 'uploads'))
    },
    filename: function (req, file, cb) {
        console.log("I'm here!!!!===============================");
        let fileName = file.fieldname + '-' + Date.now() + path.extname(file.originalname);
        console.log(`fileName ${fileName}`);
        cb(null, fileName)
    }
});

const upload = multer({storage: storage});

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://supchat-65619.firebaseio.com",
    storageBucket: "supchat-65619.appspot.com"
});

function errorHandler(err, req, res, next) {
    console.error(err.stack);
    res.status(500);
    res.send('error', {error: err});
}

app.get('/hello', async function (req, res) {
    res.send('Hello my friend!');
});

let transporter = nodemailer.createTransport({
    host: 'smtp.mailgun.org',
    port: 587,
    secure: false,
    auth: {
        user: 'postmaster@supchat.chat',
        pass: 'a2f460b03fbba1fb25da4f96b75c89b8-a65173b1-f93c40d4'
    }
});

app.get('/send-test-email', async function (req, res) {
    let result = await transporter.sendMail({
        from: '"Node js" <i@supchat.chat>',
        to: "vanya.phytsyk@gmail.com",
        subject: "Message from Node js",
        text: "This message was sent from Node js server.",
        html: '<div>' +
        '    <ol>' +
        '        <li>' +
        '            Download our application for' +
        '            <a href="https://play.google.com/store/apps/details?id=com.google.android.projection.gearhead&hl=uk"' +
        '               target="_blank">Android</a>' +
        '            or' +
        '            <a href="https://play.google.com/store/apps/details?id=com.google.android.projection.gearhead&hl=uk"' +
        '               target="_blank">iOS</a></li>' +
        +
            '        <li>Enter your email address and the <span class="il">temporary</span> <span class="il">password</span>.</li>' +
        '        <li>After logging in, you will need to provide a new <span class="il">password</span>.</li>' +
        '        <li>Use your user ID for our services access</li>' +
        '    </ol>' +
        +
            '    <p>If there\'s anything we can do to improve your experience, let us know.' +
        +
            '    </p>' +
        '    <p>SupChat Services</p>' +
        '    <p>Email: i@supchat.chat</p>' +
        +
            '    <p><a href="https://www.supchat.chat" target="_blank">SupChat</a></p>' +
        '</div>'
    });

    console.log(result);
    res.send('success')
});


app.post('/create-client', async function (req, res) {
    let user = await  admin.auth().createUser({});

    //save user in firestore
    let customerId = req.query.customer_id;
    let clientsPath = `customers/${customerId}/users`;
    let userDocumentPath = admin.firestore().collection(clientsPath).doc(user.uid);
    const shortName = uniqueNamesGenerator({
        dictionaries: [adjectives, names],
        length: 2,
        style: 'capital',
        separator: ' '
    });
    await userDocumentPath.set({
        id: user.uid,
        timestamp: FieldValue.serverTimestamp(),
        nickname: shortName
    }, {
        merge: true
    });

    let chatsPath = `customers/${customerId}/chats`;
    let chatDocumentPath = admin.firestore().collection(chatsPath).doc(user.uid);
    await chatDocumentPath.set({
        nickname: shortName,
        justCreated: true
    }, {
        merge: true
    });

    let geo = getGeo(req);
    if (geo) {
        let geoProperty = `${geo.city}, ${geo.country}`;
        let detailsPath = admin.firestore().collection(`customers/${customerId}/users/${user.uid}/details`);
        let userDetails = {value: geoProperty, type: 'location'};
        await detailsPath.doc().set(userDetails);
        for (let bodyKey in req.body) {
            await detailsPath.doc().set({type: bodyKey, value: req.body[bodyKey]})
        }
    }

    console.log(`/create-user: customerId: ${customerId}, userId: ${user.uid} , request Body: ${JSON.stringify(req.body)}`);
    res.send(user.uid);
});

app.post('/create-customer', async function (req, res) {
    try {
        let userEmail = req.body.userEmail;
        console.log(`/create-customer:  email ${userEmail}`);

        let existingUser = await admin.auth().getUserByEmail(userEmail);
        if (existingUser) {
            console.log(`/create-customer: customerId: ${JSON.stringify(existingUser)} email ${userEmail}`);
            res.send(existingUser);
            return;
        }

        let password = generatePassword();
        let user = await admin.auth().createUser({
            email: userEmail,
            emailVerified: false,
            password: password,
        });
        console.log(`/create-customer: customerId: ${JSON.stringify(user)} email ${userEmail}`);

        let emailBody = `<div>
    <div>
        <img src="assets/img/supchat_logo.svg">
    </div>
    <div>
        <p>
            Welcome Dear user, </p>
        <p>
            Thanks for contacting us regarding your SupChat account. Below, you'll find the temporary password you can use to log in at SupChat application.</p>
    </div>

    <div>
        <p>Temporary Password: ${password}</p>
        <p>Your API KEY: ${user.uid}</p>
    </div>
    <div>
        <ol>
            <li>
                Download our application for
                <a href="https://play.google.com/store/apps/details?id=com.iph.supchat"
                   target="_blank">Android</a>
                or
                <a href="https://apps.apple.com/ua/app/supchat/id1555928518"
                   target="_blank">iOS</a></li>

            <li>Enter your email address and the <span class="il">temporary</span> <span class="il">password</span>.
            </li>
            <li>After logging in, you will need to provide a new <span class="il">password</span>.</li>
            <li>Use your user ID for our services access</li>
        </ol>

        <p>If there's anything we can do to improve your experience, let us know.

        </p>
        <p>SupChat Services</p>
        <p>Email: i@supchat.chat</p>

        <p><a href="https://www.supchat.chat" target="_blank">SupChat</a></p>
    </div></div>`;

        let result = await transporter.sendMail({
                from: '"Supchat" <i@supchat.chat>',
                to: userEmail,
                subject: "SupChat registration",
                text: "Hello from SupChat",
                html: emailBody
            })
        ;


        res.send(user);
    } catch (error) {
        res.send(error);
    }
});


function generatePassword() {
    const length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let retVal = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

app.get('/getChatHistory', async function (req, res) {
    let customerId = req.query.customer_id;
    let clientId = req.query.client_id;

    console.log(`/getChatHistory, customerId: ${customerId}, userId: ${clientId}`);

    let messagesPath = `customers/${customerId}/chats/${clientId}/messages`;
    let messages = await admin.firestore().collection(messagesPath).get();
    let response = messages.docs.map(doc => {
        return doc.data();
    });
    res.send(response);
});

app.post('/sendMessage', async function (req, res) {
    let messageBody = req.body;
    console.log(`/sendMessage ${messageBody}`);
    messageBody.timestamp = FieldValue.serverTimestamp();
    messageBody.type = parseInt(messageBody.type);

    let customerId = req.query.customer_id;
    let clientId = req.query.client_id;

    //add new message to collection
    let messagesPath = `customers/${customerId}/chats/${clientId}/messages`;
    // await admin.firestore().collection(messagesPath).add(messageBody);
    let newMessageRef = admin.firestore().collection(messagesPath).doc();
    console.log(`/sendMessage, customerId: ${customerId}, userId: ${clientId}, message: ${JSON.stringify(messageBody)},
     messagePath: ${messagesPath}, newMessageRef: ${JSON.stringify(newMessageRef.id)}`);
    await newMessageRef.set(messageBody);

    //update last message in chat
    let chatPath = `customers/${customerId}/chats/${clientId}`;

    await admin.firestore().doc(chatPath).set({
        'unreadMessagesCount': FieldValue.increment(1),
        'lastMessage': messageBody,
        'justCreated': false
    }, {
        merge: true
    });


    res.sendStatus(200);
});

app.post('/sendImage', upload.single('image'), async function (req, res) {
    console.log(`/sendImage start ${JSON.stringify(req.body)}`);
    let customerId = req.body.customer_id;
    const file = req.file;
    console.log(`/sendImage customerId: ${customerId} file: ${file.path}, ${file.name}, ${file.body}`);

    if (!customerId) {
        res.sendStatus(500);
        return;
    }

    // let imageRef = admin.storage().ref().child(`${customerId}/${Date.now().toString()}`);
    // let url = await imageRef.put(req.file);

    let files = fs.readdirSync('/');
    console.log(`sendImage fileLIst ${JSON.stringify(files)}`);

    let [url] = await admin.storage().bucket().upload(file.path, {
        destination: `${customerId}/${file.originalname}`,
        public: true
    });

    let resultUrl = url.metadata.mediaLink;
    console.log(`/sendImage uploadedUrl: ${JSON.stringify(resultUrl)}`);

    res.send(resultUrl);
});

app.get('/geo', async function (req, res) {

    let geo = getGeo(req);

    console.log(geo);

    res.send(geo || 'no_geo');
});

function getGeo(req) {
    let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    return geoip.lookup(ip);
}

io.on('connection', function (socket) {
    try {
        let customerId = socket.handshake.query.customer_id;
        let clientId = socket.handshake.query.client_id;
        console.log(`a user connected, params: customer: ${customerId}, client: ${clientId}`);

        //user becomes online
        updateOnlineStatus(customerId, clientId, true);

        let messagesPath = `customers/${customerId}/chats/${clientId}/messages`;
        let now = Timestamp.now();
        let unsubsribeFromMessages = admin.firestore().collection(messagesPath)
            .where('timestamp', '>', now) //query only new messages
            .onSnapshot(querySnapshot => {
                querySnapshot.docChanges().forEach(change => {
                    let data = change.doc.data();
                    if (change.type === 'added') {
                        console.log('message received');
                        socket.emit('new_message', data);
                    }
                });
            }, err => {
                console.log(`Encountered error: ${err}`);
            });

        let chatInfoPath = `customers/${customerId}`;
        let unsubsribeFromChatInfo = admin.firestore().doc(chatInfoPath)
            .onSnapshot(querySnapshot => {
                let data = querySnapshot.data();
                console.log(`chat info: ${data.id}`);
                socket.emit('chat_info', data);
            }, err => {
                console.log(`Encountered error: ${err}`);
            });

        socket.on('disconnect', function () {
            console.log('user disconnected');
            //user becomes offline
            updateOnlineStatus(customerId, clientId, false);

            unsubsribeFromMessages();
            unsubsribeFromChatInfo();
        });
    } catch (e) {
        console.log(e);
    }
});


function updateOnlineStatus(customerId, clientId, isOnline) {
    let onlineStatusPath = `customers/${customerId}/users/${clientId}`;
    admin.firestore().doc(onlineStatusPath).set({
        online: {
            last_changed: FieldValue.serverTimestamp(),
            state: isOnline ? 'online' : 'offline'
        }
    }, {merge: true});
}

http.listen(8181, function () {
    console.log('listening on *:8181');
});